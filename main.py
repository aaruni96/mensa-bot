import yaml
import json
import datetime
import bot
import requests
import time
import re
import logging

#instantiate logger
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('mensa')

#info logger
i_handler = logging.FileHandler('info.log')
i_handler.setLevel(logging.INFO)


#logging format
lformat = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
i_handler.setFormatter(lformat)

logger.addHandler(i_handler)

logger.info('Bot turning on!')
logger.error('Logging errors!')


def get_menu():
    #we need to request this frequently as bistro menu is included only for the current week
    try:
        r = requests.get('https://www.studierendenwerk-kaiserslautern.de/fileadmin/templates/stw-kl/loadcsv/load_db_speiseplan.php')
    except Exception as e:
        logger.error(e)
        print(e)

    text = json.loads(r.text)

    class menuItem:
        name = ''
        description = ''
        date = ''
        location = ''
        zutaten = ''
        ftype = ''
        price = 0

        def __init__(self,name, description, zutaten, date, location, sublocation):
            self.name = name
            self.description = description
            self.zutaten = [z.strip() for z in zutaten.split(',')]
            self.date = datetime.datetime.strptime(date, '%Y-%m-%d')
            self.location = locations[f'{location} at {sublocation}']
            #evaluate a test for vegan / vegetarian / nonveg
            vegetarian = False
            nonveg = False
            for z in self.zutaten:
                #todo: consult what is vegan
                if z in ['Ei', 'La']:
                    vegetarian = True
                elif z in ['S', 'G', 'R', 'Fi']:
                    nonveg = True
            if (nonveg):
                self.ftype = "Non-Veg"
            elif(vegetarian):
                self.ftype = "Vegetarian"
            else:
                self.ftype = "Vegan"

    locations = {'Mensa Uni Lager at Essen I Drehkreuz/Ampel': 'Ausgabe 1 ',
                'Mensa Uni Lager at Essen II Drehkreuz/Ampel': 'Ausgabe 2',
                'Cafeteria Atrium Lager at Essen Mensa feelgood': 'Atrium Feelgood',
                'Mensa Uni Lager at Essen Mensa feelgood': 'Atrium Feelgood',
                'Cafeteria Atrium Lager at Warme Theke': 'Atrium Warm Food',
                'Bistro 36 at Essen II Drehkreuz/Ampel': 'Bistro 36',
                'Cafeteria Atrium Lager at Suppen/Eintöpfe': 'Atrium Soup'}

    dports = ['Mensa Uni Lager', 'Cafeteria Atrium Lager', 'Bistro 36']

    res = [key for key in locations]

    menu = []
    biglegend = {}

    for food in text:
        if (food['dportname'] in dports):
            menu.append(menuItem(food['dpname'], f"{food['atextohnezsz1']} {food['atextohnezsz2']} {food['atextohnezsz3']} {food['atextohnezsz4']} {food['atextohnezsz5']}", food['zsnummern'], food['proddatum'], food['dportname'], food['artname1']))
            zuts = [ z.split('=') for z in food['zsnumnamen'].split(',')]
            for z in zuts:
                if(len(z) > 1):
                    biglegend[z[0].strip()] = z[1].strip()

    menu = sorted(menu, key=lambda h: ((h.date), (h.location)))

    return menu, biglegend

def weekly(menu):
    now = datetime.datetime.now()
    curmenu = []
    days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

    for food in menu:
            #upcoming menu, delineated at weeek
        if(now.isocalendar()[2] < 6):
            if(( food.date.isocalendar()[1] == now.isocalendar()[1]) and (food.date.isocalendar()[2] >= food.date.isocalendar()[2] )):
                curmenu.append(food)
        else:
            #next week
            if(food.date.isocalendar()[1] == (now.isocalendar()[1]+1)):
                curmenu.append(food)

    mustr=''
    a = ', '
    weekdates = []
    legend = {}

    for food in curmenu:
        weekdates.append(food.date.weekday())

    weekdates = list(set(weekdates))

    for date in weekdates:
        mustr += f'|========================|\n|\t{days[date]}\t|\n|========================|\n'
        for food in curmenu:
            if food.date.weekday() == date:
                mustr += f'<strong>{food.location}</strong>: {food.description} ({food.ftype}) | {a.join(food.zutaten)}\n'
    return mustr

def gethelp(abot, chat):

    helpstring = "I am a simple bot which can tell you the mensa menu in TU Kaiserslautern! I support the following commands :\n - /menu"

    abot.sendMessage(chat, helpstring)

    return 0

def complain(abot, chat):

    complainstring = "I did not understand what you wanted from me. Try /help !"

    abot.sendMessage(chat, complainstring)

#import bot secrets
with open('secret.yaml', 'r') as yamlfile:
    secret = yaml.safe_load(yamlfile)

mybot = bot.bot(secret['bot_id'], secret['api_key'])

#main loop

while(1):
    inp = mybot.getUpdates()
    if(inp == 0):
        time.sleep(1)    #so that we don't endlessly burn CPU
        continue
    #for now we generate biglegend each time
    #later, we will save it to file
    menu, biglegend = get_menu()
    logger.info("Logging a query!")
    logger.info(inp)
    if not 'text' in inp['result'][0]['message'].keys():
        continue
    intext = inp['result'][0]['message']['text']
    reschat = inp['result'][0]['message']['chat']['id']
    quser = inp['result'][0]['message']['from']['username']
    if intext.startswith('/help'):
        logger.info(f'{quser} asked for help!')
        gethelp(mybot, reschat)
    elif intext.startswith('/start'):
        logger.info(f'Starting replying to f{quser}')
        pass
    elif intext.startswith('/menu'):
        logger.info(f'{quser} asked for menu!')
        logger.info(mybot.sendMessage(reschat, weekly(menu)).text)
        logger.info(f'Menu deliverd to {quser}')

    elif intext.startswith('/stop'):
        logger.error(f'{quser} asked us to stop but we can\'t!')
    else:
        complain(mybot, reschat)
