import requests
import json

class bot:
    def __init__(self,botID,api_key):
        self.botID = botID
        self.api_key = api_key

    def getUpdates(self):
        r = requests.get('https://api.telegram.org/bot%s:%s/%s'%(self.botID, self.api_key, 'getUpdates'))
        j = json.loads(r.text)
        #print(len(j['result']))
        if(len(j['result']) == 0):
            return 0
        update_id = int(j['result'][0]['update_id'])+1
        myobj = {'offset':str(update_id)}
        #we need to get updates again with a higher offset to clear the old update from the server
        requests.get('https://api.telegram.org/bot%s:%s/%s'%(self.botID, self.api_key, 'getUpdates'), data=myobj)
        return j

    def sendMessage(self,chatID,message):
        myobj = {'chat_id': chatID, 'text': message, 'parse_mode': 'HTML'}
        r = requests.get('https://api.telegram.org/bot%s:%s/%s'%(self.botID, self.api_key, 'sendMessage'), data=myobj)
        return r

    def sendAudio(self,chatID,audioFile):
        myobj = {'chat_id': chatID, 'title':title}
        files = {'audio':audioFile}
        r = requests.get('https://api.telegram.org/bot%s:%s/%s'%(self.botID, self.api_key, 'sendAudio'), data=myobj, files=files)
        return r

    def sendVido(self,chatID,videoFile):
        myobj = {'chat_id': chatID, 'title':title}
        files = {'audio':videoFile}
        r = requests.get('https://api.telegram.org/bot%s:%s/%s'%(self.botID, self.api_key, 'sendVido'), data=myobj, files=files)
        return r

    def sendPhoto(self,chatID,sendPhoto):
        myobj = {'chat_id': chatID, 'title':title}
        files = {'photo':photoFile}
        r = requests.get('https://api.telegram.org/bot%s:%s/%s'%(self.botID, self.api_key, 'sendPhoto'), data=myobj, files=files)
        return r

    def sendDocument(self,chatID,File):
        myobj = {'chat_id': chatID, 'title':title}
        files = {'document':File}
        r = requests.get('https://api.telegram.org/bot%s:%s/%s'%(self.botID, self.api_key, 'sendDocument'), data=myobj, files=files)
        return r

    def sendGif(self, chatID, gifFile):
        gifFile.seek(0)
        myobj = {'chat_id': chatID, 'title': 'wed'}
        files = {'animation':gifFile}
        r = requests.get('https://api.telegram.org/bot%s:%s/%s'%(self.botID, self.api_key, 'sendAnimation'), data=myobj, files=files)
        return r