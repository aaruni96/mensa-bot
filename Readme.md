# Mensa-Bot

Simple bot, pulls data from the same backend the [Studierendenwerk website](https://www.studierendenwerk-kaiserslautern.de/de/essen/speiseplaene), formats it and sends it over telegram.

## Todo

 - [x] Track ingredients
 - [x] Track veg/nonveg/vegan label
 - ~~[ ] Add support for registration / deregistration for many chats [wontfix : we don't need this]~~
 - [x] Add support to respond to queries
 - [ ] In case anyone adds issues